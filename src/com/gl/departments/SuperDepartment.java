package com.gl.departments;

/*
 * This is Super class which must be extend with all department
 */
public class SuperDepartment {
	public String departmentName() {
		return "Super Department";
	}

	public String getTodaysWork() {
		return "No Work as of now";
	}

	public String getWorkDeadline() {
		return "Nil";
	}

	public String isTodayAHoliday() {
		return "Today is not a holiday";
	}
}
