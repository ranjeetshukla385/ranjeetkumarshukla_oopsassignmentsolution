package com.gl.controller;

import com.gl.departments.AdminDepartment;
import com.gl.departments.HrDepartment;
import com.gl.departments.TechDepartment;

public class DepartmentController {

	public static void main(String[] args) {
		// Admin department
		AdminDepartment adminDepartmentObj = new AdminDepartment();
		System.out.println("Welcome to " + adminDepartmentObj.departmentName());
		System.out.println(adminDepartmentObj.getTodaysWork());
		System.out.println(adminDepartmentObj.getWorkDeadline());
		System.out.println(adminDepartmentObj.isTodayAHoliday());

		// Add some spacing between output
		System.out.println();

		// HR department
		HrDepartment hrDepartmentObj = new HrDepartment();
		System.out.println("Welcome to " + hrDepartmentObj.departmentName());
		System.out.println(hrDepartmentObj.doActivity());
		System.out.println(hrDepartmentObj.getTodaysWork());
		System.out.println(hrDepartmentObj.getWorkDeadline());
		System.out.println(hrDepartmentObj.isTodayAHoliday());

		// Add some spacing between output
		System.out.println();

		// Tech department
		TechDepartment techDepartmentObj = new TechDepartment();
		System.out.println("Welcome to " + techDepartmentObj.departmentName());
		System.out.println(techDepartmentObj.getTodaysWork());
		System.out.println(techDepartmentObj.getWorkDeadline());
		System.out.println(techDepartmentObj.getTechStackInformation());
		System.out.println(techDepartmentObj.isTodayAHoliday());
	}
}
